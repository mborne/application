var _scope = null;
var appl = angular.module("biggerapp", ["firebase"]);

//appl.config(['$routeProvider','angularFire', function($routeProvider, angularFire) {
  appl.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {templateUrl: 'templates/dashboard.php', controller: 'App'});
    //$routeProvider.when('/devis/:id_devis', {templateUrl: 'templates/devis_single.php', controller: 'Devis', resolve: { collection: angularFire("https://maxatomik.firebaseio.com/devis") }});
    $routeProvider.when('/devis/:id_devis', {templateUrl: 'templates/devis_single.php', controller: 'Devis',
      });
    $routeProvider.when('/devis/modify/:id_devis', {templateUrl: 'templates/devis_modify.php', controller: 'Devis',
      });
    $routeProvider.otherwise({redirectTo: '/home'});
  }]);

var refDevis = new Firebase("https://maxatomik.firebaseio.com/devis");
var refCdp = new Firebase("https://maxatomik.firebaseio.com/cdp");


appl.controller( 'AuthCtrl', ['$scope','angularFire', function($scope, angularFire ) {
   //Connexion grâce à facebook.
      $scope.login = function(){
        auth.login('facebook');
      },
      //Deconnexion de facebook.
     $scope.logout = function(){
        auth.logout();
      }
 }]);


FileUploadCtrl.$inject = ['$scope'];
function FileUploadCtrl(scope) {
    //============== DRAG & DROP =============
    var dropbox = document.getElementById("dropbox")
    scope.dropText = 'Drop files here...'

    // init event handlers
    function dragEnterLeave(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        scope.$apply(function(){
            scope.dropText = 'Drop files here...'
            scope.dropClass = ''
        })
    }
    dropbox.addEventListener("dragenter", dragEnterLeave, false)
    dropbox.addEventListener("dragleave", dragEnterLeave, false)
    dropbox.addEventListener("dragover", function(evt) {
        evt.stopPropagation()
        evt.preventDefault()
        var clazz = 'not-available'
        var ok = evt.dataTransfer && evt.dataTransfer.types && evt.dataTransfer.types.indexOf('Files') >= 0
        scope.$apply(function(){
            scope.dropText = ok ? 'Drop files here...' : 'Only files are allowed!'
            scope.dropClass = ok ? 'over' : 'not-available'
        })
    }, false)
    dropbox.addEventListener("drop", function(evt) {
        console.log('drop evt:', JSON.parse(JSON.stringify(evt.dataTransfer)))
        evt.stopPropagation()
        evt.preventDefault()
        scope.$apply(function(){
            scope.dropText = 'Drop files here...'
            scope.dropClass = ''
        })
        var files = evt.dataTransfer.files
        if (files.length > 0) {
            scope.$apply(function(){
                scope.files = []
                for (var i = 0; i < files.length; i++) {
                    scope.files.push(files[i])
                }
            })
        }
    }, false)
    //============== DRAG & DROP =============
    scope.setFiles = function(element) {
    scope.$apply(function(scope) {
      // Turn the FileList object into an Array
        scope.files = []
        for (var i = 0; i < element.files.length; i++) {
          scope.files.push(element.files[i])
        }
      });
    };

    scope.uploadFile = function() {
        var fd = new FormData()
        for (var i in scope.files) {
          console.log(scope.files[i]);
            fd.append("uploadedFile[]", scope.files[i])
        }
        console.log(fd);
        var xhr = new XMLHttpRequest()
        xhr.upload.addEventListener("progress", uploadProgress, false)
        xhr.addEventListener("load", uploadComplete, false)
        xhr.addEventListener("error", uploadFailed, false)
        xhr.addEventListener("abort", uploadCanceled, false)
        xhr.open("POST", "http://localhost/app2/inc/upload.php")
        xhr.send(fd)
    }

    function uploadProgress(evt) {
        NProgress.start();
    }
    function uploadComplete(evt) {
        NProgress.done();
        console.log(evt.target.responseText);
    }
    function uploadFailed(evt) {
        alert("There was an error attempting to upload the file.")
    }
    function uploadCanceled(evt) {
        alert("The upload has been canceled by the user or the browser dropped the connection.")
    }
}




//Controller Home
function App($scope, $routeParams, angularFire) {
  NProgress.start();
  var collectionCdp = angularFire(refCdp, $scope, 'cdps', {});
  var collectionDevis = angularFire(refDevis, $scope, 'devis', {} ).then(function() {
    
      _scope = $scope;
      //Connexion grâce à facebook.
      /*$scope.login = function(){
        auth.login('facebook');
      },*/
      //Deconnexion de facebook.
      /*$scope.logout = function(){
        auth.logout();
      },*/
        $scope.fileNameChaged = function()
         {
              console.log("select file"+$("#file").val());
         },
         $scope.selectFile = function()
         {
              $("#file").click();
         }
      $scope.addDevis = function(article) {
        //On vérifie que l'utilisateur à bien numéro de devis
        if (!$scope.numero_devis.length) {
          return;
        }
        console.log($scope.cdp_id);
        //On push dans la base le nouveau devis
        $scope.devis[refDevis.push().name()] = {
          numero_devis: $scope.numero_devis,
          montant_devis: $scope.montant_devis,
          cdp_id: $scope.cdp_id
        };
        //On réinitializa les champs.
        $scope.numero_devis  = $scope.montant_devis = $scope.cdp_id = "";
      },
      //Méthode de suppression de devis
      $scope.removeDevis = function(self) {
        delete $scope.devis[self];
      }
  });
NProgress.done();
}

//Controller Devis
function Devis($scope, $routeParams, angularFire) {
   NProgress.start();
  //On récup les infos d'un devis.
  var id = $routeParams.id_devis;
  var collectionCdp = angularFire(refCdp, $scope, 'cdps', {});
  var collectionDevis = angularFire(refDevis, $scope, 'devis', {} ).then(function() {
    if(typeof $scope.devis[id] !== "undefined"){
      $scope.numero_devis = $scope.devis[id].numero_devis;
      $scope.montant_devis = $scope.devis[id].montant_devis;
      $scope.id_devis = id;
      //On créer un array qui va contenir tous mes CDP
      $scope.itemSelected = new Array();
      //On loop pour recuperer les cdp associé a un devis
      console.log($scope.devis[id].cdp_id);
      for(cdp in $scope.devis[id].cdp_id){
        console.log($scope.cdps[$scope.devis[id].cdp_id[cdp].id]);
        //On push de notre nouvelle variable les CDP dans le scope CDP gràce a l'id récuprer dans le scope devis.
          $scope.itemSelected.push($scope.cdps[$scope.devis[id].cdp_id[cdp].id]);
      }
    }
    $scope.items = new Array();
    for(selected in $scope.cdps){
        $scope.items.push($scope.cdps[selected]);
    }
NProgress.done();
    $scope.addDevis = function(article) {
      //On vérifie que l'utilisateur à bien numéro de devis
      if (!$scope.numero_devis.length) {
        return;
      }
      
      console.log($scope.cdp_id);
      //On push dans la base le nouveau devis
      $scope.devis[refDevis.push().name()] = {
        numero_devis: $scope.numero_devis,
        montant_devis: $scope.montant_devis,
        cdp_id: $scope.cdp_id
      };
      //On réinitializa les champs.
      $scope.numero_devis  = $scope.montant_devis = $scope.cdp_id = "";
    },
    $scope.updateDevis = function(id) {
      //On vérifie si l'utilisateur à rempli un champs
        //Si oui on verifie que ce champs n'est pas vide, et qu'il n'est pas identique au champs d'avant
        if($scope.numero_devis != "" && typeof $scope.numero_devis !== "undefined" && $scope.numero_devis != $scope.devis[id].numero_devis){
          $scope.devis[id].numero_devis = $scope.numero_devis;
        }
        //Si oui on verifie que ce champs n'est pas vide, et qu'il n'est pas identique au champs d'avant
        if($scope.montant_devis != "" && typeof $scope.montant_devis !== "undefined" && $scope.montant_devis != $scope.devis[id].montant_devis){
          $scope.devis[id].montant_devis = $scope.montant_devis;
        }
        $scope.devis[id].cdp_id = $scope.itemSelected;
    },
    //Méthode de suppression de devis
    $scope.removeDevis = function(self) {
      delete $scope.devis[self];
    }
  });
}



//Controller Chef de Projet
function ChefDeProjet($scope, angularFire) {
  var ref = new Firebase("https://maxatomik.firebaseio.com/cdp");
  var collectionDevis = angularFire(ref, $scope, 'cdps', {} ).then(function() {
    $scope.add = function(article) {
      $scope.cdps[ref.push().name()] = {
        id: ref.push().name(),
        nom: $scope.nom,
        prenom: $scope.prenom,
        email: $scope.email
      };
    };
    console.log($scope.cdps['-J0nwwhK40S-SqqoX3TC']);
  });
}

var chatRef = new Firebase('https://maxatomik.firebaseio.com/user');
var auth = new FirebaseSimpleLogin(chatRef, function(error, user) {
  if (error) {
    // an error occurred while attempting login
    console.log(error);
  } else if (user) {
    // user authenticated with Firebase
    console.log('User ID: ' + user.id + ', Provider: ' + user.provider);

    $.getJSON('https://graph.facebook.com/'+ user.id +'/', function(data) {
      userInfo = new Array();
        for(d in data){
          userInfo[d] = data[d];
        }
        $("body").prepend("<img src='https://graph.facebook.com/"+userInfo['username']+"/picture'/><p>"+userInfo['first_name']+"</p>");
        

      });
  } else {
    userInfo = [];
    userInfo = "";
  }
});

function testIfInDOM(article, node) {
  if (node.childNodes.length != 7) return false;
  if (node.childNodes[1].checked != devis.completed) return false;
  if (node.childNodes[3].value != devis.title) return false;
  return true;
}