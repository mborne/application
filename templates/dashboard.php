<style>
.dropbox {
    width: 15em;
    height: 3em;
    border: 2px solid #DDD;
    border-radius: 8px;
    background-color: #FEFFEC;
    text-align: center;
    color: #BBB;
    font-size: 2em;
    font-family: Arial, sans-serif;
}
.dropbox span {
    margin-top: 0.9em;
    display: block;
}
.dropbox.not-available {
    background-color: #F88;
}
.dropbox.over {
    background-color: #bfb;
}
</style>
<div ng-controller="AuthCtrl">
  <button ng-click="login()">Se connecter</button>
  <button ng-click="logout()">Se déconnecter</button>
</div>
<div class="container-fluid">
  <div class="row-fluid">
    <div class="span12">
      <?php include("../inc/menu.php"); ?>
    </div>
    <div class="span12">
      
      <a id="modal-927804" href="#modal-container-927804" role="button" class="btn" data-toggle="modal">Ajouter un devis</a>
      <div id="modal-container-927804" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
         <h3 id="myModalLabel">
          Biggerapp - Ajouter un devis
        </h3>
      </div>
      <form ng-submit="addDevis()">
        <div class="modal-body">
          <fieldset>
           <label>Numéro du devis</label>

           <input type="text" placeholder="Numéro du devis"  ng-model="numero_devis" autofocus><span class="help-block">Indiquer ici le numéro du devis.</span>
           <input type="text" placeholder="Numéro du devis" ng-model="montant_devis" autofocus><span class="help-block">Indiquer ici le montant du devis.</span>
           <select multiple ng-model="cdp_id">
            <option ng-cloak ng-repeat="(key,val) in cdps" value="{{key}}">{{val.nom}}</option>
          </select>
        </fieldset>
      </div>
      <div class="modal-footer">
       <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> <button class="btn btn-primary" type="submit">Save changes</button>
     </div>
   </form>
 </div>
 <p>
  Search: <input ng-model="searchText">
</p>
<!-- UPLOAD IMAGE  -->
<div ng-controller="FileUploadCtrl">
<form enctype="multipart/form-data" method="post">
  <div class="row">
    <label for="fileToUpload">Select a File to Upload</label><br />
    <input type="file" ng-model-instant id="fileToUpload" multiple onchange="angular.element(this).scope().setFiles(this)" />
  </div>
  <div id="dropbox" class="dropbox" ng-class="dropClass"><span>{{dropText}}</span></div>
  <div ng-show="files.length">
    <div ng-repeat="file in files.slice(0)">
      <span>{{file.webkitRelativePath || file.name}}</span>
      (<span ng-switch="file.size > 1024*1024">
      <span ng-switch-when="true">{{file.size / 1024 / 1024 | number:2}} MB</span>
      <span ng-switch-default>{{file.size / 1024 | number:2}} kB</span>
    </span>)
  </div>
  <input type="button" ng-click="uploadFile()" value="Upload" />
</div>
</form>
</div>
<!-- FIN UPLOAD IMAGE  -->
<table class="table">
  <thead>
    <tr>
      <th> N/Devis </th>
      <th> Montant </th>
      <th> Action </th>
    </tr>
  </thead>
  <tbody>
    <tr ng-cloak ng-repeat="(key,val) in devis | filter : searchText">
      <td > <a href="./#/devis/{{key}}">{{val.numero_devis}}</a> </td>
      <td> {{val.montant_devis}}   </td>
      <td>
        <a href="./#/devis/{{key}}" role="button" class="btn" data-toggle="modal">Voir le devis</a>
        <button ng-click="removeDevis(key)">Supprimer le devis</button>
      </td>
    </tr>
  </tbody>
</table>
</div>
</div>
</div>