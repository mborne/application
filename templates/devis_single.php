<div ng-controller="AuthCtrl">
  <button ng-click="login()">Se connecter</button>
</div>
<div class="container-fluid">
    <div class="row-fluid">
      <div class="span12">
        <?php include("../inc/menu.php"); ?>
      </div>
      <div class="span12">
        <button ng-click="login()">Se connecter</button>
        <button ng-click="logout()">Se déconnecter</button>
      </div>
      <a id="modal-{{id_devis}}" href="#modal-container-{{id_devis}}" role="button" class="btn" data-toggle="modal">Modifier le devis</a>
      <div id="modal-container-{{id_devis}}" class="modal hide fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <!--<iframe width="100%" height="600px" frameborder="0" scrolling="no" allowtransparency="true" src="/app2/#/devis/modify/{{id_devis}}"></iframe>-->
            <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
             <h3 id="myModalLabel">
              Biggerapp - Modifier un devis
            </h3>
          </div>
          <form ng-submit="updateDevis(id_devis)">
            <div class="modal-body">
              <fieldset>
               <label>Numéro du devis : </label>
               <input placeholder="{{numero_devis}}" type="text" ng-model="numero_devis"  autofocus><span class="help-block">Indiquer ici le numéro du devis.</span>
               <label>Montant : </label>
               <input placeholder="{{montant_devis}}" type="text" ng-model="montant_devis"  autofocus><span class="help-block">Indiquer ici le montant du devis.</span>
               <label>Chef de projet : </label>
               <select size="10" id="myselection" multiple ng-multiple="true" ng-model="itemSelected" ng-options="c.nom+' ('+c.prenom+')' for c in items" ></select>
            </fieldset>
          </div>
          <div class="modal-footer">
           <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> <button class="btn btn-primary" type="submit">Save changes</button>
         </div>
       </form>
     </div>
        <p>{{numero_devis}}</p>
        <p>{{montant_devis}}</p>
        <ul>
          <li ng-cloak ng-repeat="(key,val) in itemSelected">{{val.prenom}}</li>
        </ul>
    </div>
  </div>
</div>
