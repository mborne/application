<!doctype html>
<html lang="en" ng-app="biggerapp">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Bigger App</title>
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
  <link href="css/nprogress.css" rel="stylesheet">
  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <![endif]-->
  <script type="text/javascript" src="js/jquery-1.10.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script src="lib/angular.min.js"></script>
  <script src="lib/firebase.js"></script>
  <script src="lib/angularfire.min.js"></script>
  <script type='text/javascript' src='https://cdn.firebase.com/v0/firebase-simple-login.js'></script>
  <script type='text/javascript' src='js/nprogress.js'></script>
</head>