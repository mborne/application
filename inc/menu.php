<div class="navbar">
  <div class="navbar-inner">
    <div class="container-fluid">
      <a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a> <a href="#" class="brand">BiggerApp</a>
      <div class="nav-collapse collapse navbar-responsive-collapse">
        <ul class="nav">
          <li class="active">
            <a href="./index.php">Supervision</a>
          </li>
          <li>
            <a href="#">Devis</a>
          </li>
          <li>
            <a href="#">Projet</a>
          </li>
        </ul>
        <ul class="nav pull-right" contenteditable="true">
          <li><a href="#">Me connecter</a></li>
          <li class="divider-vertical"></li>
          <li><a href="#">Me déconnecter</a></li>
          <li class="divider-vertical"></li>
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">Outils Biggerband <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>